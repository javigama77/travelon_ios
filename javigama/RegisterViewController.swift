//
//  RegisterViewController.swift
//  javigama
//
//  Created by Asap Global Solution on 08/07/2019.
//  Copyright © 2019 javigama. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController {

    @IBOutlet weak var emailEntry: UITextField!
    @IBOutlet weak var passwordEntry: UITextField!
    @IBOutlet weak var confirmpasswordEntry: UITextField!
    
    static weak var delegate : RegisterViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func createAccountAction(_ sender: Any) {
        
        let pass1count = passwordEntry.text?.count ?? 0
        let pass1 = passwordEntry.text ?? ""
        let pass2 = confirmpasswordEntry.text ?? ""

        if !isValidEmail(emailStr: emailEntry.text ?? "") {
            showAlert(message: "Introduzca un email válido", title:"Atención", dismiss:false)
        } else if pass1count < 6 {
            showAlert(message: "La contraseña debe contener al menos 6 caracteres", title:"Atención", dismiss:false)
        } else if pass1 != pass2 {
            showAlert(message: "Las contraseñas deben coincidir", title:"Atención", dismiss:false)
        } else {
            let signUpManager = FirebaseAuthManager()
            if let email = emailEntry.text, let password = passwordEntry.text {
                signUpManager.createUser(email: email, password: password) {[weak self] (success) in
                    guard let `self` = self else { return }
                    if (success) {
                        self.showAlert(message: "Usuario creado correctamente", title:"Correcto", dismiss:true)
                        RegisterViewController.delegate?.update(email)
                        //self.dismiss(animated: true, completion: nil)
                    } else {
                        self.showAlert(message: "Se ha producido un error", title:"Atención", dismiss:false)
                    }

                }
            }
        }
    }
    

}
