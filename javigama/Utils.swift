//
//  Utils.swift
//  javigama
//
//  Created by Asap Global Solution on 09/07/2019.
//  Copyright © 2019 javigama. All rights reserved.
//

import UIKit

//*****************************************************************
// MARK: - Extensions
//*****************************************************************

public extension UIColor {
    class func fuberBlue()->UIColor {
        struct C {
            static var c : UIColor = UIColor(red: 248/255, green: 44/255, blue: 4/255, alpha: 1)
        }
        return C.c
    }
    
    class func fuberLightBlue()->UIColor {
        struct C {
            static var c : UIColor = UIColor(red: 77/255, green: 181/255, blue: 217/255, alpha: 1)
        }
        return C.c
    }
}

//*****************************************************************
// MARK: - Helper Functions
//*****************************************************************

public func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
