//
//  ViewController.swift
//  javigama
//
//  Created by Asap Global Solution on 08/07/2019.
//  Copyright © 2019 javigama. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FirebaseAuth
import FBSDKLoginKit
import GoogleSignIn

protocol RegisterViewControllerDelegate: AnyObject {
    func update(_ email: String)
}

class ViewController: BaseViewController, RegisterViewControllerDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var usernameEntry: UITextField!
    @IBOutlet weak var passwordEntry: UITextField!

    @IBOutlet weak var googleButton: GIDSignInButton!
    
    private let readPermissions: [Permission] = [ .publicProfile, .email ]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RegisterViewController.delegate = self
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }

    @IBAction func loginAction(_ sender: Any) {
        let loginManager = FirebaseAuthManager()
        guard let email = usernameEntry.text, let password = passwordEntry.text else { return }
        loginManager.signIn(email: email, pass: password) {[weak self] (success) in
            guard let `self` = self else { return }
            if (success) {
                self.showAlert(message: "Bienvenido", title:"Correcto", dismiss:false)
            } else {
                self.showAlert(message: "Credenciales incorrectos", title:"Atención", dismiss:false)
            }
        }
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: readPermissions, viewController: self, completion: didReceiveFacebookLoginResult)
    }

    private func didReceiveFacebookLoginResult(loginResult: LoginResult) {
        switch loginResult {
        case .success:
            didLoginWithFacebook()
        case .failed(_): break
        default: break
        }
    }
    fileprivate func didLoginWithFacebook() {
        // Successful log in with Facebook
        if let accessToken = AccessToken.current {
            // If Firebase enabled, we log the user into Firebase
            FirebaseAuthManager().login(credential: FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)) {[weak self] (success) in
                guard let `self` = self else { return }
                if (success) {
                    self.showAlert(message: "Bienvenido", title:"Correcto", dismiss:false)
                } else {
                    self.showAlert(message: "Credenciales incorrectos", title:"Atención", dismiss:false)
                }
            }
        }
    }
    
    
    
    @IBAction func googleAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
    }
    
    func update(_ email: String) {
        usernameEntry.text = email
    }
}



