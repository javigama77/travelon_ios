//
//  SplashViewController.swift
//  javigama
//
//  Created by Asap Global Solution on 09/07/2019.
//  Copyright © 2019 javigama. All rights reserved.
//

import UIKit

open class SplashViewController: UIViewController {
    open var pulsing: Bool = false
    
    let animatedLogoView: AnimatedLogoView = AnimatedLogoView(frame: CGRect(x: 0.0, y: 0.0, width: 90.0, height: 90.0))
    var tileGridView: TileGridView!
    
    let logoView = UIImageView(image: UIImage(named: "LaunchTitle", in: Bundle(identifier: "com.proyectocice.javigama"), compatibleWith: nil))
    
    
    public init(tileViewFileName: String) {
        
        super.init(nibName: nil, bundle: nil)
        tileGridView = TileGridView(TileFileName: tileViewFileName)
        view.addSubview(tileGridView)
        tileGridView.frame = view.bounds
        
        view.addSubview(animatedLogoView)
        
        view.addSubview(logoView)

        let center = view.layer.position
        
        let screenSize = UIScreen.main.bounds
        let width = 2*screenSize.width / 9
        
        let newcenter = CGPoint(x: center.x + width, y: center.y)
        animatedLogoView.layer.position = newcenter
        
        tileGridView.startAnimating()
        animatedLogoView.startAnimating()
        
        logoView.layer.position = center

    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override var prefersStatusBarHidden : Bool {
        return true
    }
}
