//
//  TitleGridView.swift
//  javigama
//
//  Created by Asap Global Solution on 09/07/2019.
//  Copyright © 2019 javigama. All rights reserved.
//

import UIKit

class TileGridView: UIView {
    
    fileprivate var containerView: UIView!
    fileprivate var modelTileView: TileView!
    fileprivate var centerTileView: TileView? = nil
    fileprivate var numberOfRows = 0
    fileprivate var numberOfColumns = 0
    
    fileprivate var logoLabel: UILabel!
    fileprivate var logoImage: UIView!

    fileprivate var tileViewRows: [[TileView]] = []
    fileprivate var beginTime: CFTimeInterval = 0
    fileprivate let kRippleDelayMultiplier: TimeInterval = 0.0006666
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.center = center
        modelTileView.center = containerView.center
        if let centerTileView = centerTileView {
            // Custom offset needed for UILabel font
            let center = CGPoint(x: centerTileView.bounds.midX + 31, y: centerTileView.bounds.midY)
            let centerImage = CGPoint(x: centerTileView.bounds.midX, y: centerTileView.bounds.midY)
            //logoLabel.center = center
            //logoImage.center = centerImage

        }
    }
    
    init(TileFileName: String) {
        modelTileView = TileView(TileFileName: TileFileName)
        super.init(frame: CGRect.zero)
        clipsToBounds = true
        layer.masksToBounds = true
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let modelImageWidth = modelTileView.bounds.size.width
        let modelImageHeight = modelTileView.bounds.size.height
        
        let c = screenWidth / modelImageWidth
        let cc = Int(c/2)
        numberOfColumns = cc*2 + 1
        
        let r = screenHeight / modelImageHeight
        let rr = Int(r/2)
        numberOfRows = rr*2 + 1
        
        let width = Double(numberOfColumns*Int(modelImageWidth))
        let height = Double(numberOfRows*Int(modelImageHeight))
        
        containerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: width, height: height))
        containerView.backgroundColor = UIColor.black
        containerView.clipsToBounds = false
        containerView.layer.masksToBounds = false
        addSubview(containerView)

        renderTileViews()
        
        //logoLabel = generateLogoLabel()
        //logoImage = generateLogoImage()
        //centerTileView?.addSubview(logoImage)
        //centerTileView?.addSubview(logoLabel)
        layoutIfNeeded()
    }
    
    func startAnimating() {
        beginTime = CACurrentMediaTime()
        startAnimatingWithBeginTime(beginTime: beginTime)
    }
}

extension TileGridView {
    
    fileprivate func generateLogoLabel()->UILabel {
        let label = UILabel()
        label.text = "TRAVEL N"
        label.font = UIFont.systemFont(ofSize: 50)
        label.textColor = UIColor.red
        label.sizeToFit()
        label.center = CGPoint(x: bounds.midX, y: bounds.midY)
        return label
    }
    
    fileprivate func generateLogoImage()->UIImageView {

        let subView = UIImageView(image: UIImage(named: "LaunchTitle", in: Bundle(identifier: "com.proyectocice.javigama"), compatibleWith: nil))
        //subView.sizeToFit()
        subView.center = CGPoint(x: bounds.midX, y: bounds.midY)
        return subView
    }
    
    fileprivate func renderTileViews() {
        
        let modelImageWidth = modelTileView.bounds.width
        let modelImageHeight = modelTileView.bounds.height
        
        for y in 0..<numberOfRows {
            
            var tileRows: [TileView] = []
            for x in 0..<numberOfColumns {
                
                let view = TileView()
                view.frame = CGRect(x: CGFloat(x) * modelImageWidth, y:CGFloat(y) * modelImageHeight, width: modelImageWidth, height: modelImageHeight)
                
//                if view.center == containerView.center {
//                    centerTileView = view
//                }
                
                containerView.addSubview(view)
                tileRows.append(view)

                view.shouldEnableRipple = true
            }
            
            tileViewRows.append(tileRows)
        }
        
//        if let centerTileView = centerTileView {
//            containerView.bringSubviewToFront(centerTileView)
//        }
    }
    
    fileprivate func distanceFromCenterViewWithView(view: UIView)->CGFloat {
        guard let centerTileView = centerTileView else { return 0.0 }
        
        let normalizedX = (view.center.x - centerTileView.center.x)
        let normalizedY = (view.center.y - centerTileView.center.y)
        return sqrt(normalizedX * normalizedX + normalizedY * normalizedY)
    }
    
    fileprivate func normalizedVectorFromCenterViewToView(view: UIView)->CGPoint {
        let length = self.distanceFromCenterViewWithView(view: view)
        guard let centerTileView = centerTileView, length != 0 else { return CGPoint.zero }
        
        let deltaX = view.center.x - centerTileView.center.x
        let deltaY = view.center.y - centerTileView.center.y
        return CGPoint(x: deltaX / length, y: deltaY / length)
    }
    
    fileprivate func startAnimatingWithBeginTime(beginTime: TimeInterval) {
        for tileRows in tileViewRows {
            for view in tileRows {
                let distance = self.distanceFromCenterViewWithView(view: view)
                var vector = self.normalizedVectorFromCenterViewToView(view: view)
                
                vector = CGPoint(x: vector.x * 0.025 * distance, y: vector.y * 0.025 * distance)
                
                view.startAnimatingWithDuration(3, beginTime: beginTime, rippleDelay: 0.0006666 * TimeInterval(distance), rippleOffset: vector)
            }
        }
    }
}
